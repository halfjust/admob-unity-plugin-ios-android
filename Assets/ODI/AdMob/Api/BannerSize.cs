﻿namespace ODI.AdMob.Api
{
    // The size of the ad on the screen.
    public enum BannerSize
    {
        Banner,
        MediumRectangle,
        IABBanner,
        Leaderboard,
        SmartBanner
    }
}