﻿using GoogleMobileAds.Api;

namespace ODI.AdMob.Api
{
    [System.Serializable]
    public class BannerTarget
    {
		public bool enableTarget = false;
        public Gender gender = Gender.Unknown;
        public BannerTargetBirthday birthday;
        public string[] keywords;
    }
}