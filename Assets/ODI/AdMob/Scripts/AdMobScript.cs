﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using ODI.AdMob.Api;

public class AdMobScript : MonoBehaviour
{

    public string androidPublisherId = "YOUR_ANDROID_PUBLISHER_ID";
    public string iosPublisherId = "YOUR_IOS_PUBLISHER_ID";
    public BannerSize size = BannerSize.Banner;
    public AdPosition position = AdPosition.Top;
    public bool tagForChild = false; // indicará que seu conteúdo deve ser tratado como voltado para crianças de acordo com a COPPA
    public bool loadOnStart = true;
    public string[] testDeviceIds;
	public BannerTarget target;
	public bool KeepObject = true; //Makes the object target not be destroyed automatically when loading a new scene 
	
	private BannerView banner;
	
	void Awake()
	{
		if(KeepObject){
			DontDestroyOnLoad(transform.gameObject);
		}
	}

    void Start()
    {
        AdSize adBannerSize;

#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
            string adUnitId = androidPublisherId;
#elif UNITY_IPHONE
            string adUnitId = iosPublisherId;
#else
            string adUnitId = "unexpected_platform";
#endif

        //valida o tamanho do banner
        switch (size)
        {
            case BannerSize.Banner:
                adBannerSize = AdSize.Banner;
                break;
            case BannerSize.MediumRectangle:
                adBannerSize = AdSize.MediumRectangle;
                break;
            case BannerSize.IABBanner:
                adBannerSize = AdSize.IABBanner;
                break;
            case BannerSize.Leaderboard:
                adBannerSize = AdSize.Leaderboard;
                break;
            case BannerSize.SmartBanner:
                adBannerSize = AdSize.SmartBanner;
                break;
            default:
                adBannerSize = AdSize.Banner;
                break;
        }

        // cria o banner conforme parametros
        banner = new BannerView(adUnitId, adBannerSize, position);

        // Called when an ad request has successfully loaded.
        banner.AdLoaded += HandleAdLoaded;
        // Called when an ad request failed to load.
        banner.AdFailedToLoad += HandleAdFailedToLoad;
        // Called when an ad is clicked.
        banner.AdOpened += HandleAdOpened;
        // Called when the user is about to return to the app after an ad click.
        banner.AdClosing += HandleAdClosing;
        // Called when the user returned from the app after an ad click.
        banner.AdClosed += HandleAdClosed;
        // Called when the ad click caused the user to leave the application.
        banner.AdLeftApplication += HandleAdLeftApplication;

        if (loadOnStart)
        {
            //carrega dados do banner
            RequestBanner();
            //mostra
            ShowBanner();
        }
    }

    /// <summary>
    /// Efetua o request do banner
    /// </summary>
    void RequestBanner()
    {
        if (banner != null)
        {
            // Request a banner ad, with optional custom ad targeting.
            AdRequest.Builder builder = new AdRequest.Builder();
            // monta a lista de device ids
            foreach (string device in testDeviceIds)
            {
                builder.AddTestDevice(device);
            }
            //cria segmentação
            if (target != null && target.enableTarget)
            {
                builder.SetGender(target.gender);
                if (target.birthday.day > 0 && target.birthday.year > 0)
                {
                    builder.SetBirthday(new System.DateTime(target.birthday.year, (int)target.birthday.month, target.birthday.day));
                }
                foreach (string keyword in target.keywords)
                {
                    builder.AddKeyword(keyword);
                }
            }
            //indica se é conteúdo para crianças
            builder.TagForChildDirectedTreatment(tagForChild);
            //carrega informações no banner
            banner.LoadAd(builder.Build());
        }
    }

    /// <summary>
    /// Mostra banner
    /// </summary>
    void ShowBanner()
    {
        if (banner != null)
            banner.Show();
    }

    /// <summary>
    /// Esconde o banner
    /// </summary>
    void HideBanner()
    {
        if (banner != null)
            banner.Hide();
    }

    /// <summary>
    /// Destroi o banner
    /// </summary>
    void DestroyBanner()
    {
        if (banner != null)
            banner.Destroy();
    }

    #region Banner callback handlers

    public void HandleAdLoaded()
    {
        print("HandleAdLoaded event received.");
    }

    public void HandleAdFailedToLoad(string message)
    {
        print("HandleFailedToReceiveAd event received with message: " + message);
    }

    public void HandleAdOpened()
    {
        print("HandleAdOpened event received");
    }

    void HandleAdClosing()
    {
        print("HandleAdClosing event received");
    }

    public void HandleAdClosed()
    {
        print("HandleAdClosed event received");
    }

    public void HandleAdLeftApplication()
    {
        print("HandleAdLeftApplication event received");
    }

    #endregion
}
